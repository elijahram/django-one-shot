from django.urls import path
from .views import (
    todos_list,
    show_todo,
    create_todo,
    edit_todo,
    delete_todo,
    create_task,
    edit_task,
)

urlpatterns = [
    path("", todos_list, name="todo_list_list"),
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_task, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_task, name="todo_item_update"),
]
