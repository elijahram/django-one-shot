from django.shortcuts import (
    render,
    get_list_or_404,
    get_object_or_404,
    redirect,
)
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.
def create_task(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}

    return render(request, "todos/item.html", context)


def edit_task(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm(instance=todo)
    context = {"todo_object": todo, "form": form}
    return render(request, "todos/update.html", context)


def delete_todo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def edit_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm(instance=todo)
    context = {"todo_object": todo, "form": form}
    return render(request, "todos/edit.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm()
    context = {"form": form}

    return render(request, "todos/create.html", context)


def todos_list(request):
    todos = TodoList.objects.all()
    context = {"todos_list": todos}
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo}
    return render(request, "todos/details.html", context)
